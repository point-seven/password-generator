#!/usr/bin/env python

"""
Paswword Generator
v0.0.1
"""

import secrets
import string
import sys

# Checking for additional CLI arguments
# Setting password length
if len(sys.argv) == 2:
    try:
        pwd_length = int(sys.argv[1])
        print(f'Length of password defined. Generating a password with {pwd_length} characters.')
    except ValueError:
        pwd_length = 12
        print('No valid length defined. Generating a password with 12 characters.')
else:
    pwd_length = 12
    print('No length defined. Generating a password with 12 characters.')

# Defining the set of characters 
character_set = string.ascii_letters + string.digits + string.punctuation

# Generating the password
pwd = ''
for i in range(pwd_length):
    pwd += ''.join(secrets.choice(character_set))

# Printing all on terminal
print('\nHere is your password...\n')
print('{}\n'.format(pwd))
