This small Python application generates a password with a custom length out of ascii characters, digits and special characters. 
 
[[_TOC_]] 
 
# Prerequisits 
Your system has to have Python installed. 
 
## Python on Windows
Download the [installer](https://www.python.org/downloads/) and execute it. 

## Python on Mac OS
Python should already be preinstalled.

## Python on Linux
When not already installed, Python can be installed with the package installer.
 
 
# Usage
Generating a password with the default length of 12 characters. 
```bash
$ ./pw_gen.py
```

Generating a password with a custom length. In this example with 20 characters.
```bash
$ ./pw_gen.py 20
```